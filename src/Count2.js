import React, { useState } from "react";

const Count2 = () => {
	const [count, setCount] = useState(0);

	return (
		<div>
			<h4>Using useState: {count}</h4>
			<button onClick={() => setCount(count + 1)}>Add</button>
		</div>
	);
};

export default Count2;
