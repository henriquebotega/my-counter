import React from "react";

export default class Count1 extends React.Component {
	state = {
		count: 0
	};

	add = () => {
		this.setState({ count: this.state.count + 1 });
	};

	render() {
		return (
			<div>
				<h4>Using state local: {this.state.count}</h4>
				<button onClick={this.add}>Add</button>
			</div>
		);
	}
}
