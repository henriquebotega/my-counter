import React from "react";
import { useDispatch, useSelector } from "react-redux";

const Count3 = () => {
	const count = useSelector(state => state.count);
	const dispatch = useDispatch();

	return (
		<div>
			<h4>Using useSelector and useDispatch: {count}</h4>
			<button onClick={() => dispatch(count + 1)}>Add</button>
		</div>
	);
};

export default Count3;
